<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;

class ProductController extends Controller
{
    protected $products;

    public function __construct(Product $products)
    {
        $this->products = $products;
    }

    public function index()
    {
        $products = $this->products->latest()->paginate();
        return view('products.index', compact('products'));
    }

    public function show($id)
    {
        $products = $this->products->findOrFail($id);
        return view('products.show', compact('products'));
    }

    public function store(StoreProductRequest $request)
    {
        $this->products->create($request->all());
        return redirect()->route('products.index');
    }

    public function create()
    {
        return view('products.create');
    }

    public function destroy($id)
    {
        $products = $this->products->findOrFail($id);
        $products->delete();
        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $products = $this->products->findOrFail($id);
        return view('products.edit', compact('products'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $products = $this->products->findOrFail($id);
        $products->update($request->all());
        return redirect()->route('products.index');
    }
}
