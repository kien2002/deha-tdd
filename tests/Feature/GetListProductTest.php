<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function user_can_get_products_tasks()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();
        $response = $this->get($this->getRouteProductsIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($product->name)
            ->assertSee($product->contents);
    }

    public function getRouteProductsIndex()
    {
        return route('products.index');
    }

}
