<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_delete_product_if_product_exits(): void
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();
        $response = $this->delete($this->getRouteProductDelete($product->id));

        $response->assertRedirect($this->getRouteProductIndex());
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_product_if_task_exits(): void
    {
        $product = Product::factory()->create();
        $response = $this->delete($this->getRouteProductDelete($product->id));

        $response->assertRedirect($this->getRouteLogin());
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_delete_products_if_products_not_exits()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->delete($this->getRouteProductDelete(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }


    public function getRouteProductIndex()
    {
        return route('products.index');
    }

    public function getRouteProductDelete($id)
    {
        return route('products.delete', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }

}
