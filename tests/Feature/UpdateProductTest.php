<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_see_view_update_products_form()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create()->id;

        $response = $this->get($this->getRouteProductEdit($product));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
    }


    /** @test */
    public function unauthenticated_user_can_not_see_view_update_products_form(): void
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getRouteProductEdit($product->id));
        $response->assertRedirect($this->getRouteLogin());

        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticate_user_can_update_products_if_data_validated()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create()->id;
        $data = Product::factory()->make()->toArray();

        $response = $this->put($this->getRouteProductUpdate($product), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteProductsIndex());
        $this->assertDatabaseHas('products', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_product_if_data_validated()
    {
        $product = Product::factory()->create()->id;
        $data = Product::factory()->make()->toArray();
        $response = $this->put($this->getRouteProductUpdate($product), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_data_not_validated()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();
        $data = Product::factory()->make(['name' => null])->toArray();
        $response = $this->put($this->getRouteProductUpdate($product), $data);
        $response->assertSessionHasErrors(['name']);
        $response->assertStatus(Response::HTTP_FOUND);

    }

    /** @test */
    public function authenticate_user_can_see_data_message_error_if_data_not_validated()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();
        $data = Product::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteProductEdit($product->id))->put($this->getRouteProductUpdate($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteProductEdit($product->id));
    }

    /** @test */
    public function authenticate_user_can_not_update_products_if_task_not_exits()
    {
        $this->actingAs(User::factory()->create());
        $data = Product::factory()->make()->toArray();
        $response = $this->put($this->getRouteProductUpdate(-1), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getRouteProductEdit($id)
    {
        return route('products.edit', $id);
    }

    public function getRouteProductUpdate($id)
    {
        return route('products.update', $id);
    }

    public function getRouteProductsIndex()
    {
        return route('products.index');
    }

    public function getRouteLogin()
    {
        return route('login');
    }

}
