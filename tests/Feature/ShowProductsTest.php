<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowProductsTest extends TestCase
{

    /** @test */
    public function authenticate_user_can_see_task_if_task_exist()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();
        $response = $this->get($this->getRouteProductsShow($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.show');
        $response->assertSee($product->name)
            ->assertSee($product->contents);
    }

    /** @test */
    public function unauthenticate_user_can_see_products_if_products_exist()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getRouteProductsShow($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_see_products_if_task_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getRouteProductsShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }


    public function getRouteProductsShow($id)
    {
        return route('products.show', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }

}
