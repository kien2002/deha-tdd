<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewProductTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_see_view_create_products_form(): void
    {
        // đăng nhập 1 user giả định
        $this->actingAs(User::factory()->create());
        //kiem tra ket qua tra ve
        $response = $this->get($this->getRouteProductCreate());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
    }

    /** @test */
    public function unauthenticate_user_can_not_see_view_create_products_form(): void
    {
        $response = $this->get($this->getRouteProductCreate());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_create_products(): void
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->make()->toArray();

        $response = $this->post($this->getRouteProductStore(), $product);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteProductIndex());
        $this->assertDatabaseHas('products', $product);
    }

    /** @test */
    public function unauthenticate_user_can_not_create_products(): void
    {
        $product = Product::factory()->make()->toArray();

        $response = $this->post($this->getRouteProductStore(), $product);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_create_task_if_data_not_validated(): void
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->make(['name' => null])->toArray();

        $response = $this->post($this->getRouteProductStore(), $product);
        $response->assertSessionHasErrors(['name']);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticate_user_can_see_data_message_error_if_data_not_validated(): void
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->make(['name' => null])->toArray();

        $response = $this->from($this->getRouteProductCreate())->post($this->getRouteProductStore(), $product);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteProductCreate());
    }

    /** @test */
    public function authenticated_user_can_see_error_if_name_not_name(): void
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->make(['name' => null])->toArray();

        $response = $this->from(route('products.create'))->post(route('products.store', $product));
        $response->assertRedirect(route('products.create'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_must_login_can_see_form_create(): void
    {
        $product = Product::factory()->make(['name' => null])->toArray();

        $response = $this->from($this->getRouteProductCreate())->post($this->getRouteProductStore($product));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    public function getRouteProductCreate()
    {
        return route('products.create');
    }

    public function getRouteProductStore()
    {
        return route('products.store');
    }

    public function getRouteProductIndex()
    {
        return route('products.index');
    }

}
