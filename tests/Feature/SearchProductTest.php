<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_search_product_if_data_exist(): void
    {
        $this->actingAs(User::factory()->create());
        // Tạo sản phẩm để kiểm tra
        $product = Product::factory()->create();

        // Tìm kiếm sản phẩm theo tên của sản phẩm được tạo ra ở trên
        $response = $this->get($this->getRouteProductIndex(), ['search' => $product->name]);
        // Kiểm tra kết quả trả về
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($product->name)->assertSee($product->contents);
    }

    /** @test */
    public function authenticated_user_can_not_search_product_if_data_not_exist(): void
    {
        $this->actingAs(User::factory()->create());
        // Tạo sản phẩm với tên ngẫu nhiên
        $product = Product::factory()->create();
        $searchTerm = 'product_not_exist';

        // Tìm kiếm sản phẩm theo tên không tồn tại
        $response = $this->get($this->getRouteProductIndex(), ['search' => $searchTerm]);
        // Kiểm tra kết quả trả về
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertDontSee($searchTerm);
    }

    /** @test */
    public function unauthenticated_user_cannot_search_product_if_data_exist()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getRouteProductIndex());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_search_product_by_name_and_contents()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();

        $response = $this->get($this->getRouteProductIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($product->name)->assertSee($product->contents);
    }

    public function getRouteProductIndex()
    {
        return route('products.index');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
