@extends('layouts.app')
@section('products')
    <div class="container">
        <a href="{{route('products.index')}}" class="btn btn-dark">Go Back</a>
        <div class="row justify-content-center">
            <div class="col-row-8">
                <h2>Tasks Insert</h2> <br>
                <form action="{{route('products.update',$products->id)}}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="card-header">
                        <input type="text" class="form-group" name="name" value="{{$products->name}}">
                        @error('name')
                        <span class="error text-danger" id="name-error" for="name">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="card-body">
                        <input type="text" class="form-group" name="contents" value="{{$products->contents}}">
                    </div>
                    <button class="btn btn-primary">Bam thu ma xem</button>
                </form>
            </div>
        </div>
    </div>
@endsection
