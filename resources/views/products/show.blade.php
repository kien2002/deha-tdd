@extends('layouts.app')
@section('products')
    <div class="container">
        <a href="{{route('products.index')}}" class="btn btn-dark">Go Back</a>
        <hr>
        <div class="row justify-content-center">
            @csrf
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Contents</th>
                </tr>
                <tr>
                    <th>{{$products->id}}</th>
                    <th>{{$products->name}}</th>
                    <th>{{$products->contents}}</th>
                    <th>
                        <form action="{{route('products.delete',$products->id)}}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger">delete</button>
                        </form>
                        <a class="btn btn-primary" href="{{route('products.edit', $products->id)}}">Edit</a>
                    </th>
                </tr>
            </table>
        </div>
    </div>

@endsection
