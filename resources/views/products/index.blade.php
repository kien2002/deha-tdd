@extends('layouts.app')
@section('products')
    <div class="container">
        <a href="{{route('products.create')}}" class="btn btn-dark">Create Task</a>
        <div class="row justify-content-center">
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Contents</th>
                    <th>Options</th>
                </tr>
                @foreach($products as $product)
                    @csrf
                    <tr>
                        <th>{{$product->id}}</th>
                        <th>{{$product->name}}</th>
                        <th>{{$product->contents}}</th>
                        <th>
                            <a class="btn btn-success" href="{{route('products.show',$product->id)}}">View</a>
                            <form action="{{route('products.delete',$product->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger">Delete</button>
                            </form>
                            <a class="btn btn-primary" href="{{route('products.edit', $product->id)}}">Edit</a>
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
