<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\HomeController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::name('products.')->prefix('products')
    ->group(function () {
        Route::get('/', [ProductController::class, 'index'])->name('index')->middleware('auth');
        Route::post('/', [ProductController::class, 'store'])->name('store')->middleware('auth');
        Route::get('/create', [ProductController::class, 'create'])->name('create')->middleware('auth');
        Route::get('/{id}', [ProductController::class, 'show'])->name('show')->middleware('auth');
        Route::get('/{id}/edit', [ProductController::class, 'edit'])->name('edit')->middleware('auth');
        Route::delete('/{id}', [ProductController::class, 'destroy'])->name('delete')->middleware('auth');
        Route::put('/{id}', [ProductController::class, 'update'])->name('update')->middleware('auth');
    });
